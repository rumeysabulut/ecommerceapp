# Item Listing App

The project aims to add items and list those items. You can upvote and downvote items in the list. Also, items can be sorted according to their vote counts. At most 5 items can be listed in a page, the remaining ones are listed in consecutive pages.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). The development environment has node version 14.18.0 since there are some conflicts between node-sass package and node's latest versions.


## Available Scripts

After cloning the project, you should run **yarn** in the project directory to install dependencies. Then, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

