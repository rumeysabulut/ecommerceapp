import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowDown,
  faArrowUp,
  faMinusCircle,
} from "@fortawesome/free-solid-svg-icons";
import SquareContentCard from "../SquareContentCard";
import "./ListItem.scss";

function ListItem(props) {
  const { data, upVote, downVote, onClickRemove } = props;

  return (
    <div className="list-item-container">
      <SquareContentCard vote={data.vote} />
      <div className="item-info">
        <div className="item-info-link">
          <div className="item-info-link__name">{data.name}</div>
          <div className="item-info-link__url">{`(${data.url})`}</div>
        </div>

        <div className="item-info-vote">
          <button
            className="item-info-vote__button"
            type="button"
            onClick={() => upVote && upVote(data.vote + 1)}
            data-testid="upvote-button"
          >
            <FontAwesomeIcon
              icon={faArrowUp}
              className="item-info-vote__button-arrow"
            />
            Up Vote
          </button>
          <button
            className="item-info-vote__button"
            type="button"
            onClick={() => downVote && downVote(data.vote - 1)}
            data-testid="downvote-button"
          >
            <FontAwesomeIcon
              icon={faArrowDown}
              className="item-info-vote__button-arrow"
            />
            Down Vote
          </button>
        </div>
      </div>
      <div className="remove-sign">
        <FontAwesomeIcon
          icon={faMinusCircle}
          color="#c61515"
          size="lg"
          onClick={onClickRemove}
        />
      </div>
    </div>
  );
}

ListItem.defaultProps = {
  data: {},
  upVote: undefined,
  downVote: undefined,
  onClickRemove: undefined,
};

ListItem.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    url: PropTypes.string,
    vote: PropTypes.number,
  }),
  upVote: PropTypes.func,
  downVote: PropTypes.func,
  onClickRemove: PropTypes.func,
};

export default ListItem;
