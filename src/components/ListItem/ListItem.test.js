import { render, screen } from '@testing-library/react';
import ListItem from ".";

test('renders List Item', () => {
  render(<ListItem />);
  
  expect(screen.getByTestId('upvote-button')).toBeInTheDocument();
  expect(screen.getByText('Up Vote')).toBeInTheDocument();
  
  expect(screen.getByTestId('downvote-button')).toBeInTheDocument();
  expect(screen.getByText('Down Vote')).toBeInTheDocument();
});

test('renders List Item with data', () => {
  const data = {
    name: 'google',
    url: 'www.google.com'
  }
  render(<ListItem data={data}/>);
  expect(screen.getByText('google')).toBeInTheDocument();
  expect(screen.getByText('(www.google.com)')).toBeInTheDocument();
});
