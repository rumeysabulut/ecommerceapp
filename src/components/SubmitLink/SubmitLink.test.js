import { render, screen } from '@testing-library/react';
import SubmitLink from ".";

test('renders Submit Link Card', () => {
  render(<SubmitLink />);
  expect(screen.getByTestId("submit-link-content-card")).toBeInTheDocument();
  expect(screen.getByText('SUBMIT A LINK')).toBeInTheDocument();
  // Handle click metodunun calistigini nasil kontrol edicem?
});
