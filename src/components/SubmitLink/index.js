import React from "react";
import { useHistory } from "react-router-dom";

import SquareContentCard from "../SquareContentCard";
import "./SubmitLink.scss";

function SubmitLink() {
  const history = useHistory();

  const handleClick = () => {
    history.push("/addNewLink");
  };
  return (
    <div
      className="submitlink-container"
      data-testid="submit-link-content-card"
    >
      <SquareContentCard symbol onClick={handleClick} />
      <div className="big-title">SUBMIT A LINK</div>
    </div>
  );
}

export default SubmitLink;
