import React, { useMemo } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";

import "./Pagination.scss";

function Pagination(props) {
  const { currentPageNumber, pageSize, totalSize, onChange } = props;
  const pageNumbers = useMemo(
    () =>
      new Array(Math.ceil(totalSize / (Math.abs(pageSize) || 1)))
        .fill(0)
        .map((_, index) => index + 1),
    [pageSize, totalSize]
  );
  return (
    <div className="pagination-container">
      <FontAwesomeIcon
        className="left-icon"
        icon={faAngleLeft}
        size="lg"
        onClick={
          pageNumbers.length > 1
            ? () => onChange(currentPageNumber - 1)
            : () => {}
        }
      />
      {pageNumbers.map((pageNumber) => (
        <button
          type="button"
          key={`page-${pageNumber}`}
          onClick={() => onChange(pageNumber)}
          className={`page-number ${
            currentPageNumber === pageNumber ? "active" : ""
          }`}
        >
          {pageNumber}
        </button>
      ))}
      <FontAwesomeIcon
        className="right-icon"
        icon={faAngleRight}
        size="lg"
        onClick={
          pageNumbers.length > 1
            ? () => onChange(currentPageNumber + 1)
            : () => {}
        }
      />
    </div>
  );
}

Pagination.defaultProps = {
  currentPageNumber: 0,
  pageSize: 1,
  totalSize: 0,
  onChange: () => {},
};

Pagination.propTypes = {
  currentPageNumber: PropTypes.number,
  pageSize: PropTypes.number,
  totalSize: PropTypes.number,
  onChange: PropTypes.func,
};

export default Pagination;
