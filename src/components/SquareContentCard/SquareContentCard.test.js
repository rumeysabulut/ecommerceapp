import React from 'react';
import { render, screen } from '@testing-library/react';
 
import SquareContentCard from ".";

test('renders SquareContentCard component', () => {
  render(<SquareContentCard />);
  expect(screen.getByTestId(/plus-icon/)).toBeInTheDocument();
});

test('renders SquareContentCard component with vote', () => {
  render(<SquareContentCard vote={1} />);
  expect(screen.getByTestId(/vote-count/)).toBeInTheDocument();
  expect(screen.getByText(/POINTS/)).toBeInTheDocument();
});
