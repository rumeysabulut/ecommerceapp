import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import "./SquareContentCard.scss";

function SquareContentCard(props) {
  const { vote, onClick } = props;

  return vote || vote === 0 ? (
    <div className="square-container" data-testid="vote-count">
      <div className="vote-show">
        <div className="vote-count">{vote}</div>
        <div className="text">POINTS</div>
      </div>
    </div>
  ) : (
    <button
      className="square-container"
      onClick={onClick}
      type="button"
      data-testid="plus-icon"
    >
      <FontAwesomeIcon icon={faPlus} className="symbol" />
    </button>
  );
}

SquareContentCard.defaultProps = {
  vote: undefined,
  onClick: undefined,
};

SquareContentCard.propTypes = {
  vote: PropTypes.number,
  onClick: PropTypes.func,
};

export default SquareContentCard;
