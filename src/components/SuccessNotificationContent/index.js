import React from "react";
import PropTypes from "prop-types";
import "./SuccessNotificationContent.scss";

function SuccessNotificationContent(props) {
  const { name, action } = props;
  return (
    <span className="notification-content">
      <span className="notification-content__link">{name}</span> {action}.
    </span>
  );
}

SuccessNotificationContent.defaultProps = {
  name: "",
  action: "",
};

SuccessNotificationContent.propTypes = {
  name: PropTypes.string,
  action: PropTypes.string,
};

export default SuccessNotificationContent;
