import React from "react";
import ReactNotification from "react-notifications-component";
import "./Notification.scss";

function Notification() {
  return <ReactNotification className="global-notification-wrapper" />;
}

export default Notification;
