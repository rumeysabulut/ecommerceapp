import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";

const options = [
  { value: 0, label: "Most Voted" },
  { value: 1, label: "Less Voted" },
];

function DropdownSelect(props) {
  const { onSelect, selectedOrder } = props;

  return (
    <Select
      defaultValue={selectedOrder}
      onChange={onSelect}
      options={options}
      placeholder="Order by"
    />
  );
}

DropdownSelect.defaultProps = {
  onSelect: undefined,
  selectedOrder: 0,
};

DropdownSelect.propTypes = {
  onSelect: PropTypes.func,
  selectedOrder: PropTypes.number,
};

export default DropdownSelect;
