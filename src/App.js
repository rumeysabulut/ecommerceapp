import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import ItemList from "./pages/ItemList";
import AddNewLink from "./pages/AddNewLink";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/addNewLink">
          <AddNewLink />
        </Route>
        <Route path="/">
          <ItemList />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
