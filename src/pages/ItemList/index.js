import React, { useCallback, useEffect, useState } from "react";
import Modal from "react-modal";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { store } from "react-notifications-component";
import ListItem from "../../components/ListItem";
import SubmitLink from "../../components/SubmitLink";
import DropdownSelect from "../../components/DropdownSelect";
import SuccessNotificationContent from "../../components/SuccessNotificationContent";
import Pagination from "../../components/Pagination";
import "./ItemList.scss";

function ItemList() {
  const [items, setItems] = useState([]);
  const [slicedItems, setSlicedItems] = useState([]);
  const [selectedOrder, setselectedOrder] = useState(-1);
  const [showModal, setShowModal] = useState(false);
  const [nameToRemove, setNameToRemove] = useState("");
  const [itemIndexToRemove, setItemIndexToRemove] = useState(-1);

  const [pageNumber, setPageNumber] = useState(1);
  const [totalSize, setTotalSize] = useState(0);
  const [pageSize, setPageSize] = useState(5);

  const readListFromStorage = useCallback(
    () => JSON.parse(localStorage.getItem("items")) || [],
    []
  );

  useEffect(() => {
    if (items.length > 0) {
      const partialArr = items.slice(
        (pageNumber - 1) * pageSize,
        pageNumber * pageSize
      );
      setSlicedItems(partialArr);
    }
  }, [pageNumber, items, pageSize]);

  const readListIntoState = useCallback(() => {
    const newList = readListFromStorage();
    if (selectedOrder === 0) {
      // descending
      newList.sort((a, b) => {
        if (a.vote === b.vote) {
          return Date.parse(b.lastUpdate) - Date.parse(a.lastUpdate);
        }
        return b.vote - a.vote;
      });
    } else if (selectedOrder === 1) {
      // ascending
      newList.sort((a, b) => {
        if (a.vote === b.vote) {
          return Date.parse(b.lastUpdate) - Date.parse(a.lastUpdate);
        }
        return a.vote - b.vote;
      });
    } else {
      newList.sort((a, b) => {
        return Date.parse(b.lastUpdate) - Date.parse(a.lastUpdate);
      });
    }
    setItems(newList);
    setTotalSize(newList.length);
  }, [readListFromStorage, selectedOrder]);

  useEffect(() => {
    readListIntoState();
  }, [readListIntoState]);

  const handleVote = useCallback(
    (index, newVote) => {
      const newList = [...items];
      newList.splice(index, 1, {
        ...newList[index],
        vote: newVote,
        lastUpdate: new Date(),
      });
      localStorage.setItem("items", JSON.stringify(newList));
      readListIntoState();
    },
    [items, readListIntoState]
  );

  const onModalCancel = () => {
    setShowModal(false);
  };

  const showNotification = useCallback(() => {
    store.addNotification({
      content: (
        <SuccessNotificationContent name={nameToRemove} action="removed" />
      ),
      type: "success",
      insert: "top",
      container: "top-center",
      animationIn: ["animate__animated", "animate__fadeIn"],
      animationOut: ["animate__animated", "animate__fadeOut"],
      dismiss: {
        duration: 500,
      },
    });
  }, [nameToRemove]);

  const removeItem = useCallback(() => {
    const reducedList = [...items];
    reducedList.splice(itemIndexToRemove, 1);
    localStorage.setItem("items", JSON.stringify(reducedList));
    readListIntoState();
    onModalCancel();
    showNotification();
  }, [itemIndexToRemove, items, readListIntoState, showNotification]);

  const onClickRemove = (index, itemName) => {
    setShowModal(true);
    setNameToRemove(itemName);
    setItemIndexToRemove(index);
  };

  const handleSelect = (e) => {
    setselectedOrder(e.value);
  };

  return (
    <div className="home">
      <div className="home__submit-link-container">
        <SubmitLink />
      </div>
      <div className="home__separator" />
      <div className="home__dropdown-container">
        <DropdownSelect onSelect={handleSelect} selectedOrder={selectedOrder} />
      </div>
      <div className="home__item-list">
        {slicedItems.map((item, index) => (
          <ListItem
            key={`item-${item.name}`}
            data={item}
            upVote={(vote) =>
              handleVote(pageSize * (pageNumber - 1) + index, vote)
            }
            downVote={(vote) =>
              handleVote(pageSize * (pageNumber - 1) + index, vote)
            }
            onClickRemove={() => onClickRemove(index, item.name)}
          />
        ))}
      </div>
      {slicedItems.length > 0 ? (
        <Pagination
          currentPageNumber={pageNumber}
          pageSize={pageSize}
          totalSize={totalSize}
          onChange={setPageNumber}
        />
      ) : (
        <></>
      )}

      <Modal
        isOpen={showModal}
        shouldCloseOnOverlayClick
        className="home__Modal"
        overlayClassName="home__Overlay"
        onRequestClose={onModalCancel}
        ariaHideApp={false}
      >
        <div className="home__remove-modal">
          <div className="home__remove-modal-header">
            Remove Link
            <button
              className="close-icon"
              type="button"
              onClick={onModalCancel}
            >
              <FontAwesomeIcon icon={faTimes} size="lg" color="white" />
            </button>
          </div>
          <div className="home__remove-modal-content">
            <div className="home__remove-modal-content-upper">
              Do you want to remove:
            </div>
            <div className="home__remove-modal-content-lower">
              {nameToRemove}
            </div>
          </div>
          <div className="home__remove-modal-buttons">
            <button type="button" onClick={removeItem}>
              OK
            </button>
            <button type="button" onClick={onModalCancel}>
              CANCEL
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default ItemList;
