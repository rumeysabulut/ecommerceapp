import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";
import { store } from "react-notifications-component";

import SuccessNotificationContent from "../../components/SuccessNotificationContent";
import "./AddNewLink.scss";

function AddNewLink() {
  const [linkName, setLinkName] = useState("");
  const [url, setUrl] = useState("");
  const history = useHistory();

  const handleInputChange = (e) => {
    const input = e.target.name;
    if (input === "linkName") {
      setLinkName(e.target.value);
    } else {
      setUrl(e.target.value);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const savedItems = JSON.parse(localStorage.getItem("items")) || [];
    const item = {
      name: linkName,
      url,
      vote: 0,
      lastUpdate: new Date(),
    };
    localStorage.setItem("items", JSON.stringify([...savedItems, item]));
    store.addNotification({
      content: <SuccessNotificationContent name={linkName} action="added" />,
      type: "success",
      insert: "top",
      container: "top-center",
      animationIn: ["animate__animated", "animate__fadeIn"],
      animationOut: ["animate__animated", "animate__fadeOut"],
      dismiss: {
        duration: 500,
      },
    });
  };

  const returnBack = () => {
    history.replace("/");
  };
  return (
    <div className="add-link-container">
      <div>
        <button className="page-nav" type="button" onClick={returnBack}>
          <FontAwesomeIcon icon={faArrowLeft} className="return-icon" />
          <span>Return to List</span>
        </button>
        <div className="form-header">Add New Link</div>
        <form onSubmit={handleSubmit}>
          <div>
            <div className="input-label">Link Name:</div>
            <input
              className="form-input"
              name="linkName"
              type="text"
              size="50"
              value={linkName}
              onChange={handleInputChange}
              placeholder="e.g. Alphabet"
            />
          </div>
          <br />
          <div>
            <div className="input-label">Link URL:</div>
            <input
              className="form-input"
              name="linkUrl"
              type="text"
              size="50"
              value={url}
              onChange={handleInputChange}
              placeholder="e.g. http://abc.xyz"
            />
          </div>
          <input className="add-button" type="submit" value="ADD" />
        </form>
      </div>
    </div>
  );
}

export default AddNewLink;
